# Sylius Showcase Docker
This Docker image is a showcase of the [Sylius](https://sylius.com/) e-commerce platform. It comes with development dependencies but the Symfony Web Profiler's debug toolbar is disabled by default.

This uses Ubuntu 20.04 as the base image and installs the latest version of Sylius on it.

It uses supervisord to run the following services all in one Docker container:
- PHP-FPM
- Nginx
- MariaDB

### Setup
First build the docker image:
```sh
docker build --no-cache --platform linux/amd64 -t sylius-showcase:12.4.0 .
```

### Pull image (optional)

You can also pull the image in registry.
```sh
docker pull registry.gitlab.com/lsi-ufcg/cytestion/sync-study/sylius-showcase:1.12.4
```
Replace where is only `sylius-showcase:1.12.4` in the above command if you going to use this image.

### Run image

And then run a docker container with the image:
```sh
docker run -d -v $(pwd)/media:/app/public/media -v $(pwd)/sessions:/var/lib/php/sessions  -p 9990:80 --name 'sylius' sylius-showcase:1.12.4
```
>The **./media** and **./sessions** volumes is exposed for the container. We will give write permission for this folders.
>The container is exposed in port **9990**.
>The container is named as **'sylius'**.

### Add example data
```sh
docker exec sylius bin/console sylius:fixtures:load -n && sudo chown -R 33:33 media && sudo chown -R 33:33 sessions
```
>This chown permit the container to write in the media folder.
>The example data will be loaded.

### View the web shop
Visit http://localhost:9990/ to view the shop's frontend.
Visit http://localhost:9990/admin to view the admin-view.

### Login
_Default credentials:_  
Username: `sylius`  
Password: `sylius`  